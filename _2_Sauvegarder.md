# Etape 2 : Sauvegarder son travail, et travailler à plusieurs

Une fois que vos services sont actifs, il est possible de commencer à les utiliser pour coder. Ensuite, deux enjeux se posent :
- sauvegarder le code
- le partager avec le reste de l'équipe

La démarche est la même dans les deux cas : **sauvegarder sur GIT votre code**.

## Comment sauvegarder sur GIT ?

Lorsque vous ouvrez votre service, et faites le lien avec GIT, votre service (RStudio ou Jupyter) récupère les données de votre repo GIT. Cependant, lorsque vous modifiez vos fichiers depuis RStudio par exemple, la modification n'est pas instantanément transmise sur GIT : il faut le faire explicitement, au moment que vous souhaitez.

Ce travail s'effectue en trois instructions :
- "add" : cette instruction demande à votre logiciel (RStudio ou l'invite de commande) d'identifier ce qui a changé dans votre répertoire depuis votre dernière sauvegarde (add car "ajoute à ce qui doit être mis à jour).
- "commit" : cette instruction prépare l'envoi avec tous les fichiers identiifés dans l'étape précédente
- "push" : cette instruction "pousse" le paquet vers le serveur GIT, et sauvegarde donc officiellement les données.

Pour que votre travail soit véritablement sauvegardé, il est nécessaire que ces trois instructions soient réalisées. Si on voulait faire une analogie avec l'envoi d'une lettre par la poste (cela existe encore), on pourrait dire que : (i) "add" correspond au moment où vous préparez ce que vous voulez envoyer ; (ii) "commit" au moment où vous mettez les documents dans l'enveloppe ; (iii) au moment où vous posez la lettre à la poste.

Pour réaliser ces différentes instructions, tout dépend de votre environnement de travail.

### Sous Jupyter

Sous Jupyter, l'idée est simplement de retourner dans l'invite de commande qui vous a permis de charger les données (le "git pull" qui a initialisé le répertoire), et d'appliquer les instructions. On fait ainsi les 3 choses suivantes :
- "git add ." : le . veut dire, on ajoute tout ce qui a changé
- "git commit -m 'Message' " : quand on commit, il faut donner un message pour se souvenir de ce qui a changé dans ce commit. Vous pouvez écrire tout ce que vous voulez dans ce message, mais il faut mettre quelque chose.
- "git push" : on pousse vers le serveur.

**Attention !** Lorsque vous faites le git clone sur le répertoire, le répertoire de travail n'est pas le bon (c'est toujours le répertoire de référence, soit la work). Il faut donc d'abord prévenir l'invite de commande qu'on rentre dans le répertoire de travail (ici funathon-team-jordan) avec l'instruction "cd" (pour changement de répertoire, change directory).

![gitlab](images/img-12.png)

### Sous RStudio

Si tout se passe bien, vous devriez voir apparaître dans votre environnement Rstudio, les fichiers dans la fenêtre en bas à droite. Dans la fenêtre, en haut à droite, l’onglet **git** vous permet d’effectuer les opérations git simplement. Le **git add** s’obtient en cochant les fichiers modifiés que l’on souhaite sauvegarder. Le bouton **commit** permet de configurer le texte du commit et enfin le bouton **push** correspond au **git push**.

![rstudio](images/im3.PNG)

L'ensemble du processus se retrouve dans la vidéo [ici](https://youtu.be/d6z_-WWJf3Y)

## Comment travailler en équipe avec GIT ?

La démarche présentée jusqu'à présent permet de sauvegarder régulièrement son travail sur un serveur distant. C'est extrêmement précieux - et nous ne pouvons que recommander de systématiser cette pratique -. En l'état cependant, ce n'est pratique que tout seul, sur un seul ordinateur.

En équipe cependant, c'est également très utile, avec l'ajout d'une seule instruction : "pull". 

L'instruction "pull" permet de récupérer la version actuelle sur le serveur distant. Si vous travaillez en équipe, cela permet notamment de récupérer sur votre session de travail ce que vos collègues ont réalisé. Grâce à celle-ci, le repo de travail contiendra les travaux de toute l'équipe.

Pour la mettre en oeuvre, sous RStudio, il suffit de cliquer dans l'onglet GIT sur la flèche pointant vers le bas. Sous Jupyter, il suffit de taper "git pull" dans l'invite de commande. 

**Il est à noter que si vous travaillez en équipe, l'instruction pull doit être systématiquement lancée avant la sauvegarde de votre travail (elle est donc à ajouter avec le git add . présenté dans la partie précédente.)**

### Et si nous sommes deux à travailler sur le même fichier en même temps ?

De cette façon, il est possible que vous soyez, dans l'équipe, deux à travailler en même temps sur un même programme. Dans GIT, cela peut se gérer, grâce à la notion de "branches".

Dans le cadre du funathon, et si vous découvrez GIT à cette occasion, nous recommandons de ne pas vous lancer dans la création de branches. Conceptuellement, créer une branche revient à créer une version alternative de votre programme principal. Vous reprenez  tout le programme principal, mais tout ce que vous faites - y compris le git push - ne change pas la version principale de votre programme, mais seulement la branche, la version alternative. Ceci demande cependant, à un moment, que les branches soient fusionnées afin de n'avoir qu'un seul programme, qu'une seule version principale. Ce n'est pas extrêmement complexe, mais cela rajoute à notre sens une difficulté non nécessaire à la prise en main de GIT dans le cadre de ce funathon. Pour ceux qui souhaiteraient s'y frotter malgré tout, beaucoup de ressources existent sur internet (par exemple [ici](https://git-scm.com/book/fr/v2/Les-branches-avec-Git-Les-branches-en-bref)).

Dans le cadre du funathon, nous recommandons donc que vous suffixiez ou préfixiez les programmes avec vos initiales si vous êtes plusieurs à travailler ensemble, et que vous vous mettiez d'accord ensemble manuellement pour fusionner vos versions le cas échéant. Comme il s'agit d'un évènement de formation, cela fait sens de garantir un temps d'échange au moment de la fusion de vos travaux !
