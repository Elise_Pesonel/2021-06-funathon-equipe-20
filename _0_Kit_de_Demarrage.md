# Ressources pour démarrer

Les ressources pour démarrer sont regroupées dans les documents du répertoire. Pour bien les lister :

- Le document "_1_SeLancer.md" est le premier document à regarder : il détaille les modalités de prise en main de la plateforme, jusqu'à se placer en position de pouvoir coder.
- Le document "_2_Travailler.md" illustre les intéractions avec Git dans le cadre d'un travail en équipe
- Le document "_3_Gererlesfichiers.md" illustre la façon de gérer les fichiers dans le DataLab.

A noter qu'il s'agit chaque fois d'introduction à chacun des sujets (notamment pour Git, ou la gestion des fichiers).


# Rappel sur l'organisation du Funathon.

Les deux présentations réalisées jusqu'à présent ont été copiées dans le répertoire "Présentations" du Repo.

Pour rappel sur les principaux messages cependant :
- nous avons proposé 8 sujets, chacun ayant vocation à bénéficier d'un notebook de référence avec éléments de démarrage (chargement des données, premières exploitations, références extérieures) ;
- L'idée est que chaque groupe tente d'avancer sur le(s) sujet(s) qu'il s'est choisi à partir du materiel disponible (ou d'éventuel materiel supplémentaire). Ceci permet de bien s'approprier les méthodes, en se confrontant directement au problème et en essayant de le résoudre. 

- Evidemment, le materiel peut ne pas être suffisant, pas clair, ou vous pouvez vous heurter à des difficultés au moment de la mise en oeuvre. Nous viendrons régulièrement aux nouvelles pendant le Funathon mais dans ce cas, il est important de nous contacter, afin que nous puissions vous aider ! Pour cela : (i) idéalement, si c'est pertinent, en utilisant le canal Slack du sujet (pour que tous puissent en profiter) ; (ii) si le sujet vous semble peu intéressant pour le reste des participants, en utilisant le canal Slack de votre équipe mis à disposition ; nous pourrons alors échanger via ce canal, ou en vous contactant plus directement (via Zoom notamment)

Pour rappel, les salons sont déjà accessibles sur Slack (cf. vidéo [ici](https://youtu.be/Hld9Ntg3YTI) pour un tutoriel rapide)


A l'issue de la deuxième journée, une réunion de discussion collective sera mise en place sur zoom. Ceci nous permettra de partager un peu les résultats que nous avons pu voir, et à tous de s'exprimer sur l'évènement. Nous vous ferons également passer sans doute un questionnaire pour que vous puissiez nous indiquer ce qui vous a semblé utile, ou moins utile, et que nous puissions mieux préparer la prochaine fois - si une prochaine fois semble opportune -.

# Rappel sur ce que vous pouvez faire avant lundi :

Comme discuté lors de la réunion du 15 juin, vous pouvez faire différentes choses qui nous aideraient beaucoup d'ici lundi :
- vérifier que vous êtes bien inscrit sur le fichier des participants : https://docs.google.com/spreadsheets/d/1hlzQ8ARg5VsfGY_3mDBtXDyJjU0cavnj0tys-53OMes/edit#gid=0
- le compléter avec votre ou vos sujets d'intérêts, regrouper par équipe si c'est le cas, et sinon signaler votre intérêt éventuel pour rejoindre une équipe
- créer votre compte sur le datalab (les documents détaillés plus haut peuvent vous aider)

Merci par avance !

