# Sujet 2 : Où sont localisés les logements AirBnb ?

Objectif : caractériser la répartition spatiale des logements AirBnb

Quels sont les facteurs socio-économiques qui expliquent la localisation des logements Airbnb ? Comment comparer les concentrations des logements Airbnb entre les trois villes disponibles (Paris, Lyon et Bordeaux) ?

Nous vous recommandons pour les aspects cartographiques de consulter le notebook : Comment faire une carte à partir de données ponctuelles sous R ou Python.

## Pour débuter

-   Faire une carte descriptive de la localisation des logements
-   Analyser cette localisation au prisme des données carroyées : contexte économique et social
-   Comparer cette localisation avec d’autres sources externes : lieux des hôtels, lieux des monuments, lieux des restaurants et des terrasses éphémères, localisation des transactions immobilières


## Comment poursuivre ?

- Utiliser l'algorithme de Machine Learning, Dbscan pour délimiter les clusters de logements.
- Comparer la localisation des clusters des Airbnb avec celle des terrasses... 
- Utiliser des méthodes de statistiques spatiales pour comparer les distributions. 

## Ressources externes

-   [Wikipedia Dbscan](https://fr.wikipedia.org/wiki/DBSCAN)
-   <https://datascientest.com/machine-learning-clustering-dbscan>
-   [Manuel d'analyse spatiale](https://www.insee.fr/fr/information/3635442)  : Chapitre 4. Les configurations de points (section 4.5).
-   [Que nous dit l’offre Airbnb sur l’évolution des territoires touristiques ? Le cas de La Rochelle/Île de Ré](https://journals.openedition.org/mappemonde/739)
