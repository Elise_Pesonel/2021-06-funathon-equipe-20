# Sujet 3 : Que nous apprend l’analyse des sentiments des clients AirBnb ?

**Objectif** : évaluer les sentiments des clients AirBnb

- Caractériser un commentaire comme positif ou négatif
- Comparer, par logement, la note moyenne de la base à celle obtenue à partir
de l’analyse des commentaires
- Analyser la tonalité des commentaires en fonction du sexe (à partir du
prénom)
- Analyser l’évolution temporelle des sentiments
- Comparer les sentiments moyens entre les 3 villes principales

