# Sujet 5 : A quel prix puis-je louer mon appartement, ou quelle évolution des prix de location ?

Il s'agit ici de prédire le prix journalier de logements Airbnb sur la base des caractéristiques disponibles (nombre de pièces, localisation, informations sur les disponibilités, commentaires des visiteurs, etc.).

De nombreux modèles peuvent être appliquées pour effectuer cette tâche, parmi lesquels on trouve la régression linéaire, les forêts aléatoires, les Gradient Boosted Trees ou encore les réseaux de neurones.

La variable à prédire est le prix choisi et proposé par l’hôte pour son logement à un instant donné. Il est à noter que cette variable comporte une part de subjectivité et ne prend en compte ni les variations de prix ni le fait que le logement ait été effectivement loué à ce prix ou soit resté vacant.

## Pour débuter

- Repérer la variable à prédire
- Choisir quelques variables explicatives parmi les colonnes disponibles
- Faire un peu de nettoyage sur les variables sélectionnées
- Lancer un premier modèle (une régression linéaire, un arbre de décision ou une forêt aléatoire par exemple)


## Comment poursuivre ?
- Tester d'autres modèles
- Faire varier les hyperparamètres
- Ajouter des variables (notamment des variables catégorielles) et/ou améliorer les variables existantes (feature engineering)
- Gérer les valeurs manquantes
- Interpréter les résultats du modèle (importance des variables, par exemple)
- Ajouter la dimension temporelle et créer un indice de prix moyen
- Créer une interface visuelle intégrant le modèle de prédiction retenu

## Ressources externes

### Posts de blog

#### [Prédiction des prix Airbnb à Édimbourg](https://towardsdatascience.com/predicting-airbnb-prices-with-machine-learning-and-location-data-5c1e033d0a5a) - 2019
* Utilisation de l’API Foursquare pour obtenir la localisation de commodités telles que les restaurants, supermarchés, etc. et de OpenStreetMap pour le réseau routier
* Particularité de ce post : ajout d’une feature mesurant l’accessibilité à des commodités (en l’occurrence la mesure retenue est la distance à la 5e plus proche commodité). Peut-être quelques points un peu techniques pour calculer cette feature.
* Modèles : Régression hédonique (régression linéaire) et Gradient Boosted Trees (Xgboost)
* Résultats : Xgboost est meilleur. Les features les plus importantes sont : logement entier ou partagé, nombre de personnes, type de logement, nombre de salles de bain (proxy de la taille du logement ?). Les features en rapport avec les commentaires des visiteurs précédents sont assez importantes, en tout cas plus que les features géographiques (accessibilité des commodités, quartier). L'auteur du post précise que c’est à nuancer car l’étude concerne Edimbourg où on trouve assez facilement des commodités à faible distance donc ces features sont logiquement moins discriminantes dans ce contexte.
* Pas d’affinage des hyperparamètres
* Pistes d’améliorations proposées : Faire du NLP (notamment analyse de sentiment) sur les commentaires, analyser la qualité des photos des annonces avec du deep learning

#### [Prédiction des prix Airbnb à Londres](https://towardsdatascience.com/predicting-airbnb-prices-with-machine-learning-and-deep-learning-f46d44afb8a6) - 2019
* Particularités : exemple d’implémentation d’un réseau de neurones avec Tensorflow
* Modèles : Gradient Boosted Trees (Xgboost) et réseaux de neurones (Tensorflow)
* Résultats : Le modèle Xgboost fonctionne mieux que le réseau de neurones. Les features les plus importantes sont le nombre de personnes possible, le montant des frais de ménage, le nombre total d’annonces de l’hôte ou encore le nombre de dates encore disponibles dans les 90 prochains jours.
* Travail d’affinage sur le réseau de neurones pour corriger le surapprentissage initial (notamment ajout d’une régularisation L1 et de dropout)
* Pistes d’améliorations proposées : incorporer la notion de qualité des photos de l’annonce dans les features (ex : avec un réseau de neurones convolutif), considérer une zone géographique plus grande, faire du NLP sur les commentaires des visiteurs (analyse de sentiment ou recherche de mots-clés), prédire la variation saisonnière des prix en plus du prix de base

#### [Prédiction des prix Airbnb à NY](https://www.analyticsvidhya.com/blog/2020/10/predicting-nyc-airbnb-rental-prices-tensorflow/) - 2020
* Particularité : Réseau de neurone avec Tensorflow
* Tout n’est pas très clair, on dirait qu’il manque quelques étapes. Cependant on a un exemple d’implémentation d’un réseau de neurones avec relativement peu de lignes de code.

#### [Prédiction des prix Airbnb à Washington D.C.](https://www.dataquest.io/blog/machine-learning-tutorial/) - 2019
* Particularité: tutoriel particulièrement accessible à des débutants en machine learning
* Modèle : K plus proches voisins (K-nearest neighbors)
* Très guidé et progressif, l’accent est mis sur la pédagogie (tutoriel).
* En contrepartie de cette approche pédagogique, le modèle proposé est assez léger (peu de variables sont utilisées) et n’est sans doute pas le modèle le plus adapté pour atteindre d’excellentes performances de prédiction.
* Il n’y a pas vraiment de résultats à interpréter et le travail sur les hyperparamètres n’est pas abordé.

### Articles scientifiques

#### [Airbnb Price Prediction Using Machine Learning and Sentiment Analysis(Rezazded et al 2019)](https://www.researchgate.net/publication/334783073_Airbnb_Price_Prediction_Using_Machine_Learning_and_Sentiment_Analysis)
Modélisation des prix des locations Airbnb en lien avec une analyse de sentiment des commentaires.

#### [Reviews and price on online platforms: Evidence from sentiment analysis of Airbnb reviews in Boston (Lawani et al 2019)](https://www.sciencedirect.com/science/article/abs/pii/S016604621730340X)
Influence des commentaires (analyse de sentiment) sur les prix.