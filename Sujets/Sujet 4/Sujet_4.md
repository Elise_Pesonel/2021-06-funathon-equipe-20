# Le profil des loueurs influence-t-il le potentiel locatif d’un bien ?

**Objectif** : évaluer le lien entre le loueur et le potentiel locatif de son logement :

-   Obtenir une description plus complète des loueurs à partir de leurs photos : estimer le genre et l'âge du loueur à partir de l'image associée à son compte.  
-    Extraire de l’information de la présentation textuelle et du nom d'utilisateur des loueurs à l'aide des méthodes de traitement du language. Vous pouvez commencer par déterminer la langue de la présentation de l'host : anglais ou français, afin de déterminer la clientéle ciblée.  
-    Utiliser l’ensemble des informations obtenues pour évaluer l’existence d’un lien entre le profil des loueurs et les différentes variables de la base (prix, type de bien loué,...)

Pour ce sujet, seul un notebook Python est proposé. R n'est pas adapté à notre connaissance au traitement des images. Notre analyse Python repose sur la librairie [deepface](https://github.com/serengil/deepface) qui repose sur des modéles de deeplearning.

Attention, ce sujet est sensible, il doit être traité avec la plus grande prudence. En particulier, suite à la discussion pendant la présentation des sujets nous n'encourageons pas la détection de l'origine ethnique sur les images des loueurs. 

## Ressources externes

- [Airbnb prices lower among minority hosts in San Francisco](https://journalistsresource.org/economics/airbnb-discrimination-hosts/)
- [The High Cost of Short-Term Rentals in New York City, David Wachsmuth et al 2018](https://www.politico.com/states/f/?id=00000161-44f2-daac-a3e9-5ff3d8740001)
- [Article du Monde Pour réserver sur Airbnb, mieux vaut s’appeler Isabelle que Djamila](https://www.lemonde.fr/societe/article/2018/08/24/airbnb-abritel-discriminations-en-ligne_5345587_3224.html)
- [A lire absolument, la bibiothéque Python deepface pour la gender et age recognition](https://github.com/serengil/deepface)
- [Offline biases in online platforms: a study of diversity and homophily in Airbnb](https://epjdatascience.springeropen.com/articles/10.1140/epjds/s13688-019-0189-5)
- [Un exemple provenant de Kaggle](https://www.kaggle.com/gauravduttakiit/race-prediction-using-deepface-architecture)
