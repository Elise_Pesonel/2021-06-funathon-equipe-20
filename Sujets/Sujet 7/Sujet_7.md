# Quel est le profil des clients AirBnb ?

Objectif : analyser les locations effectuées par les clients AirBnb

- Classifier les clients AirBnb selon leur utilisation de la plateforme
- Visualiser finement géographiquement les zones selon le type de touriste qu’elles attirent
- Tenter de classifier et répartir les clients selon leur statut de voyageur (business ou tourisme) à partir d’une analyse de leurs commentaires et de leurs récurences spatiales.

Ce sujet n'est pas associé à des notebooks. Nous préoconisons pour commencer de consulter les notebooks dans le répertoire aides générales sur la cartographie et la détéction de langue.    

## Pour commencer :

- Cartographier les langues utilisées dans les commentaires, comparer avec les langues des annonces. 
- Discriminer les commentaires (business ou tourisme) à l'aide de méthodes de NLP : labelisation manuel d'un échantillon de commentaires puis application d'un algorithme de Machine Learning pour produire de la connaissance.
- Segmenter les clients selon leurs récurences spatiales.


## Ressources supplémentaires

- [Guest satisfaction analysis of Airbnb in Calvía and Andratx in 2016 and hotel comparison](https://www.google.com/url?sa=t&rct=j&q=&esrc=s&source=web&cd=&ved=2ahUKEwi6rbng3J7xAhWvzIUKHdYPArcQFnoECAoQAA&url=https%3A%2F%2Fdspace.uib.es%2Fxmlui%2Fbitstream%2Fhandle%2F11201%2F153170%2FMagnier_Maura_David.pdf%3Fsequence%3D1%26isAllowed%3Dy&usg=AOvVaw2_u_Abc7IX9hr3WP7bdb_O)
- [Airbnb in Paris: quel impact sur l'industrie hôtelière? - HAL-SHS](https://www.google.com/url?sa=t&rct=j&q=&esrc=s&source=web&cd=&ved=2ahUKEwj9hvnxyqHxAhVi6uAKHR00BAwQFnoECCcQAA&url=https%3A%2F%2Fhalshs.archives-ouvertes.fr%2Fhalshs-01838059%2Fdocument&usg=AOvVaw0fw0mymxobRczR-a8Wa9X3)


