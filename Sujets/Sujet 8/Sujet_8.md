# Sujet 8 : Quelles informations les données nous apportent-elles sur le suivi de l’activité touristique ?

Le but de ce sujet est de construire un tableau de bord de suivi de l'activité touristique. Celui-ci pourra éventuellement se focaliser sur une zone ou une ville en particulier.

Il s'agit dans un premier temps de calculer différents indicateurs ayant une dimension temporelle, puis dans un second temps de les intégrer dans un tableau de bord.

Ce notebook vise à vous permettre de démarrer sur ce sujet avec le chargement des données et la création d'un premier indicateur. Des idées et des ressources externes sont également proposées pour vous permettre d'aller plus loin.

## Pour débuter

- Penser à un indicateur de suivi de l'activité touristique (par exemple le nombre de réservations)
- Vérifier si l'information existe dans les données, sinon trouver un proxy (pour l'indicateur ci-dessus, le nombre de commentaires laissés par les clients est un bon proxy, à un facteur multiplicatif près) et calculer l'indicateur
- Le représenter graphiquement


### Comment poursuivre ?

- Calculer d'autres indicateurs
- Eventuellement les comparer à des sources externes
- Créer un tableau de bord pour intégrer les indicateurs
- Ajouter de l'interactivité dans le tableau de bord (choix d'une ville ou d'une période donnée par exemple)

### Ressources externes

#### Pour Python

[Documentation de Streamlit](https://docs.streamlit.io/en/stable/index.html)

Streamlit est un outil de création de tableaux de bord interactifs en Python.

#### Pour R

- La documentation Shiny : https://shiny.rstudio.com
- Un tutorial pour bien démarrer : https://thinkr.fr/a-decouverte-de-shiny/ 
- Le code d'une application assez détaillée est mis à disposition dans le répertoire "exemple d'application". Cet exemple tourne ici :  https://aurelien.shinyapps.io/Effectifs/. Pour comprendre le code, il faut avoir en tête que : (i) App.R gère l'ensemble ; (ii) Server et Ui ont été décomposés en deux répertoires différents. L'application comportant plusieurs onglets, l'idée était de pouvoir mieux organiser le code en créant un fichier par onglet, et par server/UI. C'est pratique quand l'application grandit, pas forcément pour faire un peu de datavisualisation rapidement.

Des détails supplémentaires, notamment un exemple d'application, sont proposés dans le dossier 2021-06-funathon/Aides générales/Créer Shiny
