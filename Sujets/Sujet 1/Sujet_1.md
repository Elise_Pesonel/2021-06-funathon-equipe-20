# Sujet 2 : A quoi ressemble un logement AirBnb ?

Objectif : caractériser le parc de logements AirBnb

Quels types de biens sont mis en location sur AirBnb ? Le profil des biens dépend-il du quartier ? Des conditions de location ?

Les commentaires des clients, ou les descriptions des loueurs, apportent-ils des informations sur le type de bien mis en location ?

## Pour débuter

- Faire de premières staistiques descriptives sur la base de logement
- Réaliser quelques cartes afin d'observer la variabilité des données en fonction du quartier
- Réaliser de premières analyses de commentaires afin d'essayer d'extraire de l'information, ne serait-ce que visuellement (nuage de mots)

## Comment poursuivre ?

- Créer des indicatrices de présence de certains mots afin d'enrichir la base de description des logements et donc l'analyse descriptive
- Comparer les commentaires des clients et les descriptions des loueurs
- Faire un Shiny de présentation des données
