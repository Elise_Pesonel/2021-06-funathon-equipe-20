# Etape 1 - Comment se lancer ?

> Pour se familiariser avec l'environnement de travail, deux ressources principales et prioritaires sont à disposition :
> - la présentation de la DIIT du 7 juin sur le datalab qui se trouve [ici](https://minio.lab.sspcloud.fr/projet-funathon/diffusion/prez-SSPCloud-datalab.mp4)
> - la documentation officielle du datalab qui se trouve [ici](https://docs.sspcloud.fr)
>
> Le présent document ne vise qu'à compléter éventuellement ces ressources, mais c'est bien la documentation officielle qui fait foi. Pour compléter, en R, "Utiliser RStudio sur l'environnement SSPCloud" dans la documentation UtilitR [ici](https://www.book.utilitr.org/sspcloud.html)

## 1.a. Créer son compte

L'étape initiale est bien sûr de **créer son compte sur le datalab**. Pour cela, il suffit de se rendre sur l'adresse datalab.sspcloud.fr, de cliquer en haut à droite sur "connexion" puis "nouvel utilisateur", et de se laisser guider.

Il est nécessaire d'utiliser votre adresse professionnelle. Seules les adresses ayant une extension pré-enregistrée (i.e. les adresses du SSP) sont en effet autorisées. Si ce n'est pas le cas et que la création ne fonctionne pas, n'hésitez pas à nous contacter pour que nous signalons à la DIIT votre problème afin qu'ils puissent vous ouvrir l'accès.

Une fois votre compte créé sur le datalab, il faut vous rendre sur l'adresse git.lab.sspcloud.fr pour vous connecter avec vos identifiants du datalab, et vérifier ainsi que votre compte est bien activé. GitLab étant un service associé au datalab (également accessible via https://datalab.sspcloud.fr/services), il ne devrait pas y avoir de problème.

## 1.b. Ajouter un mot de passe à son compte GitLab

Gitlab est l'outil qui permet de stocker vos codes et vos fichiers en conservant la chronologie de toutes les modifications qui ont été effectuées dessus. Pour pouvoir l'utiliser à cet effet cependant, il va être nécessaire d'ajouter un mot de passe à votre compte GitLab. Pour cela, il suffit de se rendre en haut à droite dans le menu, de cliquer sur préférences, puis password (si vous avez un doute, la - courte - démarche est [ici](https://youtu.be/ALEtxkcM8Sw) en vidéo).

![gitlab](images/im1.png)

## 1.c. Créer son répertoire de travail

Les projets (projects ou repos en anglais - on parlera de **repo** dans le reste du document) sont les unités de stockage élémentaire. Globalement, c'est l'équivalent du répertoire de travail sur lequel vous travaillez habituellement lorsque vous codez, à la différence qu'il est recommandé de ne pas y stocker de données mais seulement ses codes.

Dans le cadre du funathon, nous proposons que vous commenciez le travail avec un répertoire contenant l'ensemble des codes que nous avons préparé pour vous. 

Pour réaliser cela, il va suffire de copier chez vous notre répertoire (notre "repo"). Deux éléments importants à noter cependant :
- **il suffit qu'un seul membre de l'équipe réalise cette copie**, l'idée étant qu'ensuite ce répertoire de travail soit partagé entre les différents membres pour que tous travaillent "au même endroit" (cf. plus bas pour une explication du fonctionnement) ;
- cette copie peut être réalisée dès réception de ce document. Cependant, de notre côté, le repo va évoluer jusque sans doute dimanche soir : **si vous souhaitez travailler avec notre dernière version en utilisant le procédé évoqué ici, il faudra recommencer la copie (i.e. recréer un nouveau répertoire de travail) lundi matin**


Ces deux avertissements posés, la démarche pour réaliser la copie est la suivant. Les codes qui permettent d'accéder aux données et de débuter les analyses sont présentes dans le repo <https://git.lab.sspcloud.fr/ssplab/2021-06-funathon>. Pour importer le projet funathon dans votre environnement Gitlab personnel, il suffit d'utiliser le menu **Projects/Import project** puis de selectionner  **Repo by URL** et en copiant dans le champ **Git repository URL** l’adresse du project Gitlab <https://git.lab.sspcloud.fr/ssplab/2021-06-funathon.git>. Les champs **Project name**, **Project URL** et **Project slug** sont mis à jour automatiquement.

La démarche est illustrée en vidéo [ici](https://youtu.be/3RPkoC22A3k) (précision importante suite à remarque : dans la vidéo, le repo créé s'appelle "team-jordan" - il s'agissait d'une référence au meilleur basketteur de l'histoire, nullement de politique...)

Il est à noter qu'à la fin de la création du projet, trois niveaux de confidentialité vous sont proposés : private, member, ou public. Les recommandations de la DIIT sont de placer les projets en public. Cela offre un avantage certain en termes de transparence. Dans le cadre du Funathon, aucune obligation cependant, libre à vous de choisir le niveau qui vous convient.

Dans tous les cas, il faudra cependant que le membre de l'équipe créant le projet ouvre les droits d'édition aux autres membres, afin qu'ils puissent librement interagir avec. Si le projet est privé, il serait également utile que vous ouvriez un accès au moins en lecture aux membres du SSP Lab, afin que nous puissions si nécessaire voir votre code pour vous aider à débugger.

## 1.d. Rendre accessible son répertoire de travail au reste de l'équipe

Pour ouvrir son répertoire de travail, il suffit de se rendre sur la page du repo, puis dans "Members" (dans le menu de gauche). Il vous est alors possible d'inviter des membres extérieurs à rejoindre le projet, avec 4 possibilités de droits :
- Guest
- Reporter
- Developer
- Maintainer

Le détail des droits associés à chaque rôle est présenté dans le tableau sur la page [ici](https://git.lab.sspcloud.fr/help/user/permissions). De façon simple, nous vous conseillons de donner évidemment tous les droits aux autres membres de votre équipe (donc leur donner le statut de Maintainer). Si votre projet est privé, nous vous remercions également d'ajouter les membres du SSP Lab en tant que Reporter, afin que nous puissions vous aider. 


Le détail de la démarche est présenté en vidéo [ici](https://youtu.be/xZIc4DYzE2A)

## 1.e. Créer son service pour pouvoir travailler (sous R)

### Créer son service 

Le lancement d’une instance Rstudio s’effectue à travers la fenêtre **Catalogue de services** dont l’adresse est <https://datalab.sspcloud.fr/catalog/inseefrlab-helm-charts-datascience>. En appuyant sur **Lancer** vous obtenez une fenêtre de configuration. Dans le menu déroulant **Configuration Rstudio**, vous allez devoir intervenir sur deux onglets. Dans l’onglet **Security**,  décochez le bouton **Enable IP protection** et dans l’onglet **Ressources**   augmentez la mémoire à 16192. Vous pouvez maintenant **Lancer** votre service. 

![Le datalab](images/im6.png)

Il apparaît normalement dans **Mes services** <https://datalab.sspcloud.fr/my-service>. Vous accédez à celui-ci à l’aide de la petite flèche. Avant de cliquer sur celle-ci, pensez à cliquer sur la clef bleue pour copier le password de votre service (ce n’est pas le password de la plateforme ni de Gitlab). Pour accéder à votre service Rstudio, vous devez remplir le username : rstudio, puis coller avec Ctrl+V le password.

![Le datalab](images/im7.png)

L'ensemble de cette démarche est détaillée dans la vidéo présente [ici](https://youtu.be/TUVXixdGzCc)

### Interaction avec Gitlab

Rstudio permet d'interfacer Gitlab directement à l'aide du menu **File\New Project**. Choisissez ensuite **Version Control**, puis **Git**. L’adresse du **Repository URL** est obtenue en cliquant sur le bouton **clone** bleu dans la fenêtre Gitlab de votre projet et en sélectionnant **Clone with HTPPS** (voir au-dessus). Normalement le **Project Directory Name** est automatiquement complété. Ensuite, il vous reste à cliquer sur **Browse** puis à choisir **Choose**. Il vous reste plus qu’à cliquer sur  **Create project** pour obtenir le chargement de votre projet en local.

![rstudio](images/im5.png)

Si tout se passe bien, vous devriez voir apparaître dans votre environnement Rstudio, les fichiers dans la fenêtre en bas à droite. Dans la fenêtre, en haut à droite, l’onglet **git** vous permet d’effectuer les opérations git simplement. Le **git add** s’obtient en cochant les fichiers modifiés que l’on souhaite sauvegarder. Le bouton **commit** permet de configurer le texte du commit et enfin le bouton **pull** correspond au **git pull** (l'usage de ces instructions est détaillé dans la suite du document).

![rstudio](images/im3.PNG)

L'ensemble de la démarche est présentée [ici](https://youtu.be/ekn91M_NLtw)

## 1.f. Créer son service pour pouvoir travailler (sous R ou Python, via Jupyter)

Jupyter est un outil pour créer des notebooks vous permettant d'accompagner votre code. Vous trouverez des explications générales [ici](https://fr.wikipedia.org/wiki/Jupyter) et spécifiques à Python [ici](https://python.sdv.univ-paris-diderot.fr/18_jupyter/). 

Comme pour Rstudio, on ouvre une instance Jupyter à l’aide du menu  **Catalogue de services**. Vous devez également remplir les options de lancement de la même manière. Le mot de passe est également disponible grâce à la clef.

L'ensemble de cette démarche est détaillée dans la vidéo présente [ici](https://youtu.be/emFZrvtsc30)



### Interaction avec Gitlab

Pour importer votre projet, vous devez ouvrir un terminal, puis vous devez exécuter la commande **git clone votre_projet**. L’adresse de **votre_projet** est obtenue en cliquant sur le bouton clone bleu dans la fenêtre Gitlab de votre projet (voir au-dessus) et en sélectionnant **Clone with HTTPS**. Ensuite pour sauvegarder vos travaux, vous devez exécuter les commandes **git add**, **git commit** et **git push** (l'utilisation de ces commandes est détaillé ci-dessous).

La démarche est détaillée dans la vidéo (ici)[https://youtu.be/Gf40UBhmIA8]



## Conclusion

A ce stade, vos services sont actifs, le lien avec GIT est réalisé, vous pouvez commencer à travailler et à coder.
Deux documents de compléments à signaler cependant :
- le document "_2_Sauvegarder.md" qui décrit comment gérer les interactions avec GIT une fois que vous avez du code à sauver
- le document "_3_GererLesFichiers.md" qui décrit la façon d'importer des données à vous sur le datalab, on travailler en commun sur une base de données supplémentaires





