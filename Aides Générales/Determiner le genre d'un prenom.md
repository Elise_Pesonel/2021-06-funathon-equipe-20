# Determiner le genre d'un prénom

Cette opération s'effectue simplement en téléchargant un dictionnaire de [prénom](https://www.insee.fr/fr/statistiques/2540004?sommaire=4767262#dictionnaire), puis en calculant des statistiques par prénom sur le genre associé. Un merge à votre base de données, vous permet ensuite d'obtenir des probabilités de genre !

