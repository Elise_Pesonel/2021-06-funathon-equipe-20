# Comment créer une application Shiny ?

Le package Shiny est un package extrêmement pratique sous R : il permet de créer de jolies applications, très facilement.

Nous n'avons pas vocation à proposer ici une documentation détaillée sur la meilleure façon de gérer de tels packages, simplement de vous fournir de la documentation de lancement : n'hésitez pas à nous connecter pour en discuter si nécessaire

## Grands principes

Le principe d'une application Shiny est d'être décomposée en deux éléments :
- un UI, soit une interface graphique. L'ensemble est regroupé dans une fonction "ui.R", qui recense tous les éléments graphiques nécessaires pour l'application, ou dans une fonction associée
- un server, c'est à dire une fonction qui gère les réactions à ce qui se passe sur l'interface graphique. L'ensemble de ces réactions sera stocké dans un programme "server.R"

## Références potentielles

- La documentation Shiny : https://shiny.rstudio.com
- Un tutorial pour bien démarrer : https://thinkr.fr/a-decouverte-de-shiny/ 
- Le code d'une application assez détaillée est mis à disposition dans le répertoire "exemple d'application". Cet exemple tourne ici :  https://aurelien.shinyapps.io/Effectifs/. Pour comprendre le code, il faut avoir en tête que : (i) App.R gère l'ensemble ; (ii) Server et Ui ont été décomposés en deux répertoires différents. L'application comportant plusieurs onglets, l'idée était de pouvoir mieux organiser le code en créant un fichier par onglet, et par server/UI. C'est pratique quand l'application grandit, pas forcément pour faire un peu de datavisualisation rapidement.