# Variables utiles pour l'onglet "effectifs"


  # Partie "Nature du contrat" -----------------------------------------------------------

observe({
  req(input$input_nature)
  req(input$input_nature_date)
})

observe({

  output$dt_eff_nature <- renderDT({
    df = get(paste("t_eff_nature_",input$nature_choix,sep=''))
    df = full_join(x = nomencl_nature, y = df, by = c("reduit"="Nature") )
    df$reduit = NULL
    colnames(df) = c("Nature",affichage_nom)
    df[c(2:dim(df)[2])] = format( df[c(2:dim(df)[2])] , big.mark = " ")

    datatable(df, extensions = 'Buttons', rownames = FALSE, options = list(scrollX = TRUE, pageLength = 30, searching = FALSE, rownames = FALSE, dom = 'lBftip', buttons = c('copy')))
  })
  
  output$nature_cdi <- renderValueBox({
    tempo = get(paste("t_eff_nature_",input$nature_choix,sep=''))
    valueBox(
      color = "orange",
      value = format(as.numeric(tempo[tempo$Nature == "1",][nom_last_day[which(affichage_nom==input$input_nature_date)]]), big.mark = " "),
      subtitle = paste("Effectif en CDI le", str_sub(sequence_last_day[which(affichage_nom==input$input_nature_date)],9,10), months(sequence_last_day[which(affichage_nom==input$input_nature_date)]), str_sub(sequence_last_day[which(affichage_nom==input$input_nature_date)],1,4),sep=" "),
      icon=icon("landmark", class = "ico")
    )
  })
  
    output$nature_cdd <- renderValueBox({
      tempo = get(paste("t_eff_nature_",input$nature_choix,sep=''))
      valueBox(
        color = "orange",
        value = format(as.numeric(tempo[tempo$Nature == "2",][nom_last_day[which(affichage_nom==input$input_nature_date)]]), big.mark = " "),
        subtitle = paste("Effectif en CDD le", str_sub(sequence_last_day[which(affichage_nom==input$input_nature_date)],9,10), months(sequence_last_day[which(affichage_nom==input$input_nature_date)]), str_sub(sequence_last_day[which(affichage_nom==input$input_nature_date)],1,4),sep=" "),
        icon=icon("landmark", class = "ico")
      )
    })
      
    output$nature_interim <- renderValueBox({
      tempo = get(paste("t_eff_nature_",input$nature_choix,sep=''))
        valueBox(
          color = "orange",
          value = format(as.numeric(tempo[tempo$Nature == "3",][nom_last_day[which(affichage_nom==input$input_nature_date)]]), big.mark = " "),
          subtitle = paste("Effectif en Intérim le", str_sub(sequence_last_day[which(affichage_nom==input$input_nature_date)],9,10), months(sequence_last_day[which(affichage_nom==input$input_nature_date)]), str_sub(sequence_last_day[which(affichage_nom==input$input_nature_date)],1,4),sep=" "),
          icon=icon("landmark", class = "ico")
        )
      })
    
    output$nature_graphique <- renderHighchart({
      tempo = get(paste("t_eff_nature_",input$nature_choix,sep=''))
      df = as.data.frame(cbind(tempo[,1],tempo[,which(affichage_nom==input$input_nature_date)+1]))
      colnames(df) = c("Nature","Valeur")
      df = dplyr::left_join(x = df, y = nomencl_nature, by = c("Nature" = "reduit" ))
      colnames(df) = c("Nature","Valeur",  "Nature_full")
      df = df[-c(1:3),] # On enlève les 3 natures principales (CDI, CDD, Intérim)
      
      graphique = hchart(df, "pie", hcaes(x = Nature_full, y = Valeur)) %>% hc_add_theme(hc_theme_gridlight())
        # hc_title(text = "Effectif pour les autres natures", style = list(fontSize = "15px")) %>% 
      
      return(graphique)
      })
    
    output$nature_graphique_det <- renderHighchart({
      tempo = get(paste("t_eff_nature_",input$nature_choix,sep=''))
      colnames(tempo) = c("Nature",affichage_nom)
      d = tempo[tempo$Nature==nomencl_nature[nomencl_nature$complet==as.character(input$input_nature),]$reduit,]
      d = melt(d[-c(1)])
      graphique = hchart(d, "line",hcaes(y=value, x = variable, group = 1)) %>% hc_tooltip(table = TRUE,
                                                                                       sort = TRUE,
                                                                                       pointFormat = paste0( '<br> <span style="color:{point.color}">\u25CF</span>',
                                                                                                             " {point.y}"),
                                                                                       headerFormat = '<span style="font-size: 13px">{point.key}</span>'
      )  %>% hc_add_theme(hc_theme_gridlight()) %>% hc_legend()
        # hc_legend( layout = 'vertical', align = 'left', verticalAlign = 'top', floating = T, x = 100, y = 000 ) %>% 
      return(graphique)
    })
    
    
    
})


# Partie "Dispositif de Politique Publique" -----------------------------------------------------------

observe({
  req(input$input_disp)
  req(input$input_disp_date)
})

observe({
  output$disp_cui <- renderValueBox({
    tempo = get(paste("t_eff_disp_",input$disp_choix,sep=''))
    valeur = as.numeric(tempo[tempo$Dispositif == "21",][nom_last_day[which(affichage_nom==input$input_disp_date)]]) + 
              as.numeric(tempo[tempo$Dispositif == "41",][nom_last_day[which(affichage_nom==input$input_disp_date)]]) +
              as.numeric(tempo[tempo$Dispositif == "42",][nom_last_day[which(affichage_nom==input$input_disp_date)]])
    valueBox(
      color = "orange",
      value = format(valeur, big.mark = " "),
      subtitle = paste("Effectif en CUI le", str_sub(sequence_last_day[which(affichage_nom==input$input_disp_date)],9,10), months(sequence_last_day[which(affichage_nom==input$input_disp_date)]), str_sub(sequence_last_day[which(affichage_nom==input$input_disp_date)],1,4),sep=" "),
      icon=icon("landmark", class = "ico")
    )
  })
  
  output$disp_emploi_avenir <- renderValueBox({
    tempo = get(paste("t_eff_disp_",input$disp_choix,sep=''))
    valeur = as.numeric(tempo[tempo$Dispositif == "50",][nom_last_day[which(affichage_nom==input$input_disp_date)]]) + 
      as.numeric(tempo[tempo$Dispositif == "51",][nom_last_day[which(affichage_nom==input$input_disp_date)]]) 
    valueBox(
      color = "orange",
      value = format(valeur, big.mark = " "),
      subtitle = paste("Effectif en Emplois d'avenir le", str_sub(sequence_last_day[which(affichage_nom==input$input_disp_date)],9,10), months(sequence_last_day[which(affichage_nom==input$input_disp_date)]), str_sub(sequence_last_day[which(affichage_nom==input$input_disp_date)],1,4),sep=" "),
      icon=icon("landmark", class = "ico")
    )
  })
  
  output$disp_apprentis <- renderValueBox({
    tempo = get(paste("t_eff_disp_",input$disp_choix,sep=''))
    valeur = as.numeric(tempo[tempo$Dispositif == "64",][nom_last_day[which(affichage_nom==input$input_disp_date)]]) + 
      as.numeric(tempo[tempo$Dispositif == "65",][nom_last_day[which(affichage_nom==input$input_disp_date)]]) 
    valueBox(
      color = "orange",
      value = format(valeur, big.mark = " "),
      subtitle = paste("Effectif en apprentissage le", str_sub(sequence_last_day[which(affichage_nom==input$input_disp_date)],9,10), months(sequence_last_day[which(affichage_nom==input$input_disp_date)]), str_sub(sequence_last_day[which(affichage_nom==input$input_disp_date)],1,4),sep=" "),
      icon=icon("landmark", class = "ico")
    )
  })
  
  output$disp_graphique <- renderHighchart({
    tempo = get(paste("t_eff_disp_",input$disp_choix,sep=''))
    df = as.data.frame(cbind(tempo[,1],tempo[,which(affichage_nom==input$input_disp_date)+1]))
    colnames(df) = c("Dispositif","Valeur")
    df = dplyr::left_join(x = df, y = nomencl_disp, by = c("Dispositif" = "reduit" ))
    colnames(df) = c("Dispositif","Valeur",  "Dispositif_full")
    
    graphique = hchart(df, "pie", hcaes(x = Dispositif_full, y = Valeur)) %>% hc_add_theme(hc_theme_gridlight())

    return(graphique)
  })
  
  output$disp_graphique_det <- renderHighchart({
    tempo = get(paste("t_eff_disp_",input$disp_choix,sep=''))
    colnames(tempo) = c("Dispositif",affichage_nom)
    d = tempo[tempo$Dispositif==nomencl_disp[nomencl_disp$complet==as.character(input$input_disp),]$reduit,]
    d = melt(d[-c(1)])
    
    graphique = hchart(d, "line",hcaes(y=value, x = variable, group = 1)) %>% hc_tooltip(table = TRUE,
                                                                                         sort = TRUE,
                                                                                         pointFormat = paste0( '<br> <span style="color:{point.color}">\u25CF</span>',
                                                                                                               "{point.y}"),
                                                                                         headerFormat = '<span style="font-size: 13px">{point.key}</span>'
    )  %>% hc_add_theme(hc_theme_gridlight()) %>% hc_legend()
      # hc_legend( layout = 'vertical', align = 'left', verticalAlign = 'top', floating = T, x = 100, y = 000 ) %>% 
    return(graphique)
  })
  
  output$dt_eff_disp <- renderDT({
    df = get(paste("t_eff_disp_",input$disp_choix,sep=''))
    df = full_join(x = nomencl_disp, y = df, by = c("reduit"="Dispositif") )
    df$reduit = NULL
    colnames(df) = c("Dispositif",affichage_nom)
    df[c(2:dim(df)[2])] = format( df[c(2:dim(df)[2])] , big.mark = " ")
    
    datatable(df, extensions = 'Buttons', rownames = FALSE, options = list(scrollX = TRUE, pageLength = 30, searching = FALSE, rownames = FALSE, dom = 'lBftip', buttons = c('copy')))
  })
})

# Partie "Motif de Recours" -----------------------------------------------------------

observe({
  req(input$input_motif)
  req(input$input_motif_date)
})

observe({
  output$motif_sais <- renderValueBox({
    tempo = get(paste("t_eff_motif_",input$motif_choix,sep=''))
    valeur = as.numeric(tempo[tempo$MotifRecours == "3",][nom_last_day[which(affichage_nom==input$input_motif_date)]])
    valueBox(
      color = "orange",
      value = format(valeur, big.mark = " "),
      subtitle = paste("Effectif en contrat saisonnier le", str_sub(sequence_last_day[which(affichage_nom==input$input_motif_date)],9,10), months(sequence_last_day[which(affichage_nom==input$input_motif_date)]), str_sub(sequence_last_day[which(affichage_nom==input$input_motif_date)],1,4),sep=" "),
      icon=icon("landmark", class = "ico")
    )
  })
  
  output$motif_vend <- renderValueBox({
    tempo = get(paste("t_eff_motif_",input$motif_choix,sep=''))
    valeur = as.numeric(tempo[tempo$MotifRecours == "4",][nom_last_day[which(affichage_nom==input$input_disp_date)]]) 
    valueBox(
      color = "orange",
      value = format(valeur, big.mark = " "),
      subtitle = paste("Effectif en contrat de vendange le", str_sub(sequence_last_day[which(affichage_nom==input$input_motif_date)],9,10), months(sequence_last_day[which(affichage_nom==input$input_motif_date)]), str_sub(sequence_last_day[which(affichage_nom==input$input_motif_date)],1,4),sep=" "),
      icon=icon("landmark", class = "ico")
    )
  })
  
  output$motif_usage <- renderValueBox({
    tempo = get(paste("t_eff_motif_",input$motif_choix,sep=''))
    valeur = as.numeric(tempo[tempo$MotifRecours == "5",][nom_last_day[which(affichage_nom==input$input_disp_date)]]) 
    valueBox(
      color = "orange",
      value = format(valeur, big.mark = " "),
      subtitle = paste("Effectif en contrat d'usage le", str_sub(sequence_last_day[which(affichage_nom==input$input_motif_date)],9,10), months(sequence_last_day[which(affichage_nom==input$input_motif_date)]), str_sub(sequence_last_day[which(affichage_nom==input$input_motif_date)],1,4),sep=" "),
      icon=icon("landmark", class = "ico")
    )
  })
  
  output$motif_graphique <- renderHighchart({
    tempo = get(paste("t_eff_motif_",input$motif_choix,sep=''))
    df = as.data.frame(cbind(tempo[,1],tempo[,which(affichage_nom==input$input_motif_date)+1]))
    colnames(df) = c("MotifRecours","Valeur")
    df = dplyr::left_join(x = df, y = nomencl_motif, by = c("MotifRecours" = "reduit" ))
    colnames(df) = c("MotifRecours","Valeur",  "MotifRecours_full")
    
    graphique = hchart(df, "pie", hcaes(x = MotifRecours_full, y = Valeur)) %>% hc_add_theme(hc_theme_gridlight())
    
    return(graphique)
  })
  
  output$motif_graphique_det <- renderHighchart({
    tempo = get(paste("t_eff_motif_",input$motif_choix,sep=''))
    colnames(tempo) = c("MotifRecours",affichage_nom)
    d = tempo[tempo$MotifRecours==nomencl_motif[nomencl_motif$complet==as.character(input$input_motif),]$reduit,]
    d = melt(d[-c(1)])
    
    graphique = hchart(d, "line",hcaes(y=value, x = variable, group = 1)) %>% hc_tooltip(table = TRUE,
                                                                                         sort = TRUE,
                                                                                         pointFormat = paste0( '<br> <span style="color:{point.color}">\u25CF</span>',
                                                                                                               "{point.y}"),
                                                                                         headerFormat = '<span style="font-size: 13px">{point.key}</span>'
    )  %>% hc_add_theme(hc_theme_gridlight()) %>% hc_legend()
    # hc_legend( layout = 'vertical', align = 'left', verticalAlign = 'top', floating = T, x = 100, y = 000 ) %>% 
    return(graphique)
  })
  
  output$dt_eff_motif <- renderDT({
    df = get(paste("t_eff_motif_",input$motif_choix,sep=''))
    df = full_join(x = nomencl_motif, y = df, by = c("reduit"="MotifRecours") )
    df$reduit = NULL
    colnames(df) = c("MotifRecours",affichage_nom)
    df[c(2:dim(df)[2])] = format( df[c(2:dim(df)[2])] , big.mark = " ")
    
    datatable(df, extensions = 'Buttons', rownames = FALSE, options = list(scrollX = TRUE, pageLength = 30, searching = FALSE, rownames = FALSE, dom = 'lBftip', buttons = c('copy')))
  })
})
  
  # Partie "Nationalité" -----------------------------------------------------------
  
  observe({
    req(input$input_nati)
    req(input$input_nati_date)
  })
  
  observe({
    output$nati_france <- renderValueBox({
      tempo = get(paste("t_eff_nati_",input$nati_choix,sep=''))
      valeur = as.numeric(tempo[tempo$Nationalite == "1",][nom_last_day[which(affichage_nom==input$input_nati_date)]]) 
      valueBox(
        color = "orange", value = format(valeur, big.mark = " "),
        subtitle = paste("Effectif de Nationalité française le", str_sub(sequence_last_day[which(affichage_nom==input$input_nati_date)],9,10), months(sequence_last_day[which(affichage_nom==input$input_nati_date)]), str_sub(sequence_last_day[which(affichage_nom==input$input_nati_date)],1,4),sep=" "),
        icon=icon("landmark", class = "ico")
      )
    })
    
    output$nati_ue <- renderValueBox({
      tempo = get(paste("t_eff_nati_",input$nati_choix,sep=''))
      valeur = as.numeric(tempo[tempo$Nationalite == "2",][nom_last_day[which(affichage_nom==input$input_nati_date)]]) 
      valueBox(
        color = "orange", value = format(valeur, big.mark = " "),
        subtitle = paste("Effectif de Nationalité UE le", str_sub(sequence_last_day[which(affichage_nom==input$input_nati_date)],9,10), months(sequence_last_day[which(affichage_nom==input$input_nati_date)]), str_sub(sequence_last_day[which(affichage_nom==input$input_nati_date)],1,4),sep=" "),
        icon=icon("landmark", class = "ico")
      )
    })
    
    output$nati_eee <- renderValueBox({
      tempo = get(paste("t_eff_nati_",input$nati_choix,sep=''))
      valeur = as.numeric(tempo[tempo$Nationalite == "3",][nom_last_day[which(affichage_nom==input$input_nati_date)]]) 
      valueBox(
        color = "orange", value = format(valeur, big.mark = " "),
        subtitle = paste("Effectif de Nationalité EEE le", str_sub(sequence_last_day[which(affichage_nom==input$input_nati_date)],9,10), months(sequence_last_day[which(affichage_nom==input$input_nati_date)]), str_sub(sequence_last_day[which(affichage_nom==input$input_nati_date)],1,4),sep=" "),
        icon=icon("landmark", class = "ico")
      )
    })
    
    output$nati_hue <- renderValueBox({
      tempo = get(paste("t_eff_nati_",input$nati_choix,sep=''))
      valeur = as.numeric(tempo[tempo$Nationalite == "4",][nom_last_day[which(affichage_nom==input$input_nati_date)]]) 
      valueBox(
        color = "orange", value = format(valeur, big.mark = " "),
        subtitle = paste("Effectif de Nationalité extra européenne le", str_sub(sequence_last_day[which(affichage_nom==input$input_nati_date)],9,10), months(sequence_last_day[which(affichage_nom==input$input_nati_date)]), str_sub(sequence_last_day[which(affichage_nom==input$input_nati_date)],1,4),sep=" "),
        icon=icon("landmark", class = "ico")
      )
    })
    
    output$nati_graphique_det <- renderHighchart({
      tempo = get(paste("t_eff_nati_",input$nati_choix,sep=''))
      colnames(tempo) = c("Nationalite",affichage_nom)
      d = tempo[tempo$Nationalite==nomencl_nati[nomencl_nati$complet==as.character(input$input_nati),]$reduit,]
      d = melt(d[-c(1)])
      
      graphique = hchart(d, "line",hcaes(y=value, x = variable, group = 1)) %>% hc_tooltip(table = TRUE,
                                                                                           sort = TRUE,
                                                                                           pointFormat = paste0( '<br> <span style="color:{point.color}">\u25CF</span>',
                                                                                                                 "{point.y}"),
                                                                                           headerFormat = '<span style="font-size: 13px">{point.key}</span>'
      )  %>% hc_add_theme(hc_theme_gridlight()) %>% hc_legend()
        # hc_legend( layout = 'vertical', align = 'left', verticalAlign = 'top', floating = T, x = 100, y = 000 ) %>% 
      return(graphique)
    })
    
    output$dt_eff_nati <- renderDT({
      df = get(paste("t_eff_nati_",input$nati_choix,sep=''))
      df = full_join(x = nomencl_nati, y = df, by = c("reduit"="Nationalite") )
      df$reduit = NULL
      colnames(df) = c("Nationalite",affichage_nom)
      df[c(2:dim(df)[2])] = format( df[c(2:dim(df)[2])] , big.mark = " ")
      
      datatable(df, extensions = 'Buttons', rownames = FALSE, options = list(scrollX = TRUE, pageLength = 30, searching = FALSE, rownames = FALSE, dom = 'lBftip', buttons = c('copy')))
    })
  })

  # Partie "Mode d'exercice du contrat" -----------------------------------------------------------
  
  observe({
    req(input$input_mode)
    req(input$input_mode_date)
  })
  
  observe({
    output$mode_plein <- renderValueBox({
      tempo = get(paste("t_eff_mode_",input$mode_choix,sep=''))
      valeur = as.numeric(tempo[tempo$ModeExercice == "10",][nom_last_day[which(affichage_nom==input$input_mode_date)]]) 
      valueBox(
        color = "orange", value = format(valeur, big.mark = " "),
        subtitle = paste("Effectif à temps plein le ", str_sub(sequence_last_day[which(affichage_nom==input$input_mode_date)],9,10), months(sequence_last_day[which(affichage_nom==input$input_mode_date)]), str_sub(sequence_last_day[which(affichage_nom==input$input_mode_date)],1,4),sep=" "),
        icon=icon("landmark", class = "ico")
      )
    })
    
    output$mode_partiel <- renderValueBox({
      tempo = get(paste("t_eff_mode_",input$mode_choix,sep=''))
      valeur = as.numeric(tempo[tempo$ModeExercice == "20",][nom_last_day[which(affichage_nom==input$input_mode_date)]]) 
      valueBox(
        color = "orange", value = format(valeur, big.mark = " "),
        subtitle = paste("Effectif à temps partiel le", str_sub(sequence_last_day[which(affichage_nom==input$input_mode_date)],9,10), months(sequence_last_day[which(affichage_nom==input$input_mode_date)]), str_sub(sequence_last_day[which(affichage_nom==input$input_mode_date)],1,4),sep=" "),
        icon=icon("landmark", class = "ico")
      )
    })
    
    output$mode_graphique <- renderHighchart({
      tempo = get(paste("t_eff_mode_",input$mode_choix,sep=''))
      df = as.data.frame(cbind(tempo[,1],tempo[,which(affichage_nom==input$input_mode_date)+1]))
      colnames(df) = c("ModeExercice","Valeur")
      df = dplyr::left_join(x = df, y = nomencl_mode, by = c("ModeExercice" = "reduit" ))
      colnames(df) = c("ModeExercice","Valeur",  "ModeExercice_full")
      
      graphique = hchart(df, "pie", hcaes(x = ModeExercice_full, y = Valeur)) %>% hc_add_theme(hc_theme_gridlight())
      # hc_title(text = "Effectif pour les autres natures", style = list(fontSize = "15px")) %>% 
      
      return(graphique)
    })
    
    output$dt_eff_mode <- renderDT({
      df = get(paste("t_eff_mode_",input$mode_choix,sep=''))
      df = full_join(x = nomencl_mode, y = df, by = c("reduit"="ModeExercice") )
      df$reduit = NULL
      colnames(df) = c("ModeExercice",affichage_nom)
      df[c(2:dim(df)[2])] = format( df[c(2:dim(df)[2])] , big.mark = " ")
      
      datatable(df, extensions = 'Buttons', rownames = FALSE, options = list(scrollX = TRUE, pageLength = 30, searching = FALSE, rownames = FALSE, dom = 'lBftip', buttons = c('copy')))
    })
    
    output$mode_graphique_det <- renderHighchart({
      tempo = get(paste("t_eff_mode_",input$mode_choix,sep=''))
      colnames(tempo) = c("ModeExercice",affichage_nom)
      d = tempo[tempo$ModeExercice==nomencl_mode[nomencl_mode$complet==as.character(input$input_mode),]$reduit,]
      d = melt(d[-c(1)])
      
      graphique = hchart(d, "line",hcaes(y=value, x = variable, group = 1)) %>% hc_tooltip(table = TRUE,
                                                                                           sort = TRUE,
                                                                                           pointFormat = paste0( '<br> <span style="color:{point.color}">\u25CF</span>',
                                                                                                                 "{point.y}"),
                                                                                           headerFormat = '<span style="font-size: 13px">{point.key}</span>'
      )  %>% hc_add_theme(hc_theme_gridlight()) %>% hc_legend()
      # hc_legend( layout = 'vertical', align = 'left', verticalAlign = 'top', floating = T, x = 100, y = 000 ) %>% 
      return(graphique)
    })
  })
    
    # Partie "Statut du contrat" -----------------------------------------------------------
    
    observe({
      req(input$input_statut)
      req(input$input_statut_date)
    })
    
    observe({
      output$statut_fonct <- renderValueBox({
        tempo = get(paste("t_eff_statut_",input$statut_choix,sep=''))
        valeur = as.numeric(tempo[tempo$CodeStatut == "1",][nom_last_day[which(affichage_nom==input$input_statut_date)]]) 
        valueBox(
          color = "orange", value = format(valeur, big.mark = " "),
          subtitle = paste("Effectif de fonctionnaires le ", str_sub(sequence_last_day[which(affichage_nom==input$input_statut_date)],9,10), months(sequence_last_day[which(affichage_nom==input$input_statut_date)]), str_sub(sequence_last_day[which(affichage_nom==input$input_statut_date)],1,4),sep=" "),
          icon=icon("landmark", class = "ico")
        )
      })
      
      output$statut_contr <- renderValueBox({
        tempo = get(paste("t_eff_statut_",input$statut_choix,sep=''))
        valeur = as.numeric(tempo[tempo$CodeStatut == "2",][nom_last_day[which(affichage_nom==input$input_statut_date)]]) 
        valueBox(
          color = "orange", value = format(valeur, big.mark = " "),
          subtitle = paste("Effectif de contractuels de la fonction publique le ", str_sub(sequence_last_day[which(affichage_nom==input$input_statut_date)],9,10), months(sequence_last_day[which(affichage_nom==input$input_statut_date)]), str_sub(sequence_last_day[which(affichage_nom==input$input_statut_date)],1,4),sep=" "),
          icon=icon("landmark", class = "ico")
        )
      })
      
      output$statut_graphique <- renderHighchart({
        tempo = get(paste("t_eff_statut_",input$statut_choix,sep=''))
        df = as.data.frame(cbind(tempo[,1],tempo[,which(affichage_nom==input$input_statut_date)+1]))
        colnames(df) = c("CodeStatut","Valeur")
        df = dplyr::left_join(x = df, y = nomencl_statut, by = c("CodeStatut" = "reduit" ))
        colnames(df) = c("CodeStatut","Valeur",  "CodeStatut_full")
        
        graphique = hchart(df, "pie", hcaes(x = CodeStatut_full, y = Valeur)) %>% hc_add_theme(hc_theme_gridlight())
        # hc_title(text = "Effectif pour les autres natures", style = list(fontSize = "15px")) %>% 
        
        return(graphique)
      })
      
      output$dt_eff_statut <- renderDT({
        df = get(paste("t_eff_statut_",input$statut_choix,sep=''))
        df = full_join(x = nomencl_statut, y = df, by = c("reduit"="CodeStatut") )
        df$reduit = NULL
        colnames(df) = c("CodeStatut",affichage_nom)
        df[c(2:dim(df)[2])] = format( df[c(2:dim(df)[2])] , big.mark = " ")
        
        datatable(df, extensions = 'Buttons', rownames = FALSE, options = list(scrollX = TRUE, pageLength = 30, searching = FALSE, rownames = FALSE, dom = 'lBftip', buttons = c('copy')))
      })
      
      output$statut_graphique_det <- renderHighchart({
        tempo = get(paste("t_eff_statut_",input$statut_choix,sep=''))
        colnames(tempo) = c("CodeStatut",affichage_nom)
        d = tempo[tempo$CodeStatut==nomencl_statut[nomencl_statut$complet==as.character(input$input_statut),]$reduit,]
        d = melt(d[-c(1)])
        
        graphique = hchart(d, "line",hcaes(y=value, x = variable, group = 1)) %>% hc_tooltip(table = TRUE,
                                                                                             sort = TRUE,
                                                                                             pointFormat = paste0( '<br> <span style="color:{point.color}">\u25CF</span>',
                                                                                                                   "{point.y}"),
                                                                                             headerFormat = '<span style="font-size: 13px">{point.key}</span>'
        )  %>% hc_add_theme(hc_theme_gridlight()) %>% hc_legend()
        # hc_legend( layout = 'vertical', align = 'left', verticalAlign = 'top', floating = T, x = 100, y = 000 ) %>% 
        return(graphique)
      })
    
  })