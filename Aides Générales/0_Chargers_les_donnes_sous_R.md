# Charger les données sous R

Ce notebook illustre comment charger les données du funathon sous Python. 

## Données d'Airbnb

#### Etape 0 : télécharger les données depuis internet

On commence par une étape facultative : télécharger les données depuis Internet pour les ramener sur Minio.

Cette étape est facultative aujourd'hui car le travail a déjà été fait. Cela peut être utile à terme, notamment si d'autres villes apparaissent, ou si vous souhaitez mettre à jour les données par la suite.

Pour cela, on commence par charger 3 librairies

> library(aws.s3) # pour charger les données
> library(tidyverse) # pour lire les données

On définit ensuite le lieu dans lequel il faudra stocker les données téléchargées, à savoir le serveur MINIO.

On précise l'url dans lequel se trouve les données actuellement (trouvable directement en consultant la page d'insideAirBnb)

> paris_urls = ['http://data.insideairbnb.com/france/ile-de-france/paris/2021-04-10/data/listings.csv.gz',
              'http://data.insideairbnb.com/france/ile-de-france/paris/2021-04-10/data/reviews.csv.gz']

> bordeaux_urls = ['http://data.insideairbnb.com/france/nouvelle-aquitaine/bordeaux/2021-04-18/data/listings.csv.gz',
                 'http://data.insideairbnb.com/france/nouvelle-aquitaine/bordeaux/2021-04-18/data/reviews.csv.gz']

> lyon_urls = ['http://data.insideairbnb.com/france/auvergne-rhone-alpes/lyon/2021-04-18/data/listings.csv.gz',
             'http://data.insideairbnb.com/france/auvergne-rhone-alpes/lyon/2021-04-18/data/reviews.csv.gz']
             
L'idée est ensuite simplement, pour chaque url, d'aller chercher le contenu (urllib.request.urlretrieve), puis de le copier dans Minio (fs.put)

Attention : ici, on voit qu'on ramène les données dans le répertoire "projet-funathon/diffusion/data". Ce répertoire est bien sûr à adapter selon vos besoins (à ramener plutôt chez vous ou dans un répertoire créé pour l'occasion avec l'aide de la DIIT)

> for url in paris_urls:
>    path = '' + os.path.basename(url)
>    urllib.request.urlretrieve(url, path)
>    fs.put(path, 'projet-funathon/diffusion/data/paris/'+ url.split('/')[6] + '/' + os.path.basename(url))

>for url in bordeaux_urls:
>    path = '' + os.path.basename(url)
>    urllib.request.urlretrieve(url, path)
>    fs.put(path, 'projet-funathon/diffusion/data/bordeaux/'+ url.split('/')[6] + '/' + os.path.basename(url))    
    
>for url in lyon_urls:
>    path = '' + os.path.basename(url)
>    urllib.request.urlretrieve(url, path)
>    fs.put(path, 'projet-funathon/diffusion/data/lyon/'+ url.split('/')[6] + '/' + os.path.basename(url))      


#### Etape 1 : récupérer les données de Minio pour pouvoir travailler dessus

L'étape 0 étant facultative, on reprend avec le chargement des packages nécessaires.

> library(aws.s3) # pour charger les données
> library(tidyverse) # pour lire les données

L'idée est ensuite simplement de récupérer les données de Minio pour les ramener dans l'espace de travail (l'instruction adaptée est 'save_object'). Ensuite, on peut les utiliser dans R, comme tout fichier csv classique.

La fonction save_object prend plusieurs arguments :
- "object" : c'est le chemin vers l'objet qu'on veut télécharger. Par exemple "projet-funathon/diffusion/data/paris/2021-04-10/listings.csv.gz"
- "bucket" : c'est le bucket dans lequel sont les données, c'est à dire le répertoire de référence. Ici, c'est "projet-funathon"
- "file" : le fichier est ramené dans l'espace de travail de R, l'argument "file" indique le nom qu'il aura à ce moment. Il est en effet possible de faire en sorte que le fichier ait un nom différent dans le répertoire de travail de R que son nom initial
- "region" : argument à remplir, mais à laisser à ""

save_object(object="funathon/data/paris/2021-02-08/reviews.csv.gz",bucket="ssplab",file="reviews.csv.gz", region="")
reviews = read_csv('reviews.csv.gz')


Par exemple, le listing des logements est stocké dans 'ssplab/funathon/data/listing.csv.gz'. Il est à noter que le fichier est un fichier csv, compressé avec une compression de type 'gz', ce qui doit être spécifié au moment de l'ouverture.

Et c'est tout ! Pas même d'étape 2. Pour rappel, la liste des fichiers téléchargeable par ville est :

- listings.csv.gz
- reviews.csv.gz

Le détail de l'intérêt de chaque fichier se trouve dans la description des fichiers (cf. fichier Markdown associé). A noter que le site Airbnb en propose plus, mais nous avons estimé que ces fichiers - qui sont les plus complets - étaient suffisants.