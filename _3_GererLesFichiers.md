# Etape 3 : gérer les fichiers

Cette étape est facultative. Elle ne couvre en réalité que deux cas de figure :

- (i) vous souhaitez ajouter dans votre espace de travail des fichiers extérieurs de données
- (ii) dans le cadre du travail d'un membre du groupe, vous avez créé une donnée, un tableau, que vous souhaiteriez partager avec tous

Si vous n'êtes pas dans l'un de ces cas de figure, ce document n'est pas utile pour le moment :)

Sinon, alors la suite peut sans doute vous aider !


## Le stockage de vos fichiers

Amazon Simple Storage Service ([Amazon S3](https://aws.amazon.com/fr/s3/)) est un service de stockage de fichier (il est parfois nommé **minio**). Sur le Datalab, vous pouvez explorer les fichiers disponibles dans votre espace personnel à l’aide du menu **Mes fichiers** <https://datalab.sspcloud.fr/mes-fichiers>. En cliquant sur l'un de vos répertoires disponibles (dans le monde S3, un répertoire de stockage est appelée **bucket**), vous accédez à une page vous permettant de créer un répertoire ou d’uploader un fichier sur la plateforme.

![minio](images/im8.png)

Des commandes spécifiques permettent de charger ou de sauvegarder vos fichiers directement dans vos environnements Python et R. Elles sont décrites ci-dessous.

## Interaction sous RStudio


Le package aws.s3 permet de dialoguer avec S3 :
```
libray(aws.s3)
put_object(file = "toto.R", bucket = "nom_du_bucket", object='toto.R', region="") # envoie sur un bucket le fichier toto.R
save_object(object="toto.R",bucket="nom_du_bucket",file="toto.R", region="") #  télécharge sur votre session locale le fichier toto.R contenu dans le bucket.
```

## Interaction sous Jupyter


La librairie s3fs permet de dialoguer avec S3 sous Python. Contrairement à R, vous devez paramétrer l'adresse du service S3 avec la commande : 
```
import s3fs
fs = s3fs.S3FileSystem(client_kwargs={'endpoint_url': 'https://minio.lab.sspcloud.fr'})
fs.put('toto.py', 'nom_idep/toto.py'), envoie le fichier dans le bucket associé à mon nom_idep
fs.get('mon_idep/toto.py', 'toto.py') permet de sauvegarder en local le fichier toto.py contenu dans votre bucket.   
```

## Vidéos d'explication

La procédure pour importer des données extérieures et les utiliser est détaillée en vidéo :
- pour RStudio c'est [ici](https://www.youtube.com/watch?v=hibHGplWtzM)
- Pour Jupyter c'est [ici](https://youtu.be/CpQ78v7aTJ0)

## Partage des données

Les explications ci-dessous permettent de gérer ses données pour un travail solitaire.

Pour un travail collectif, il peut être utile qu'une personne importe les données pour tous, ou plus globalement que les données qu'un des membre stocke sur son minio soit accessible à tous.

Pour cela, c'est assez simple : il faut se rendre sur l'interface du datalab, et choisir l'option "public". La petite vidéo ci-après l'illustre : [ici](https://youtu.be/qqUtUEZ25LQ)